import 'package:cloud_firestore/cloud_firestore.dart';

/// cate_image : "https://firebasestorage.googleapis.com/v0/b/sizzling-taste.appspot.com/o/category%2Fpizza-category.jpg?alt=media&token=6c1adf0d-02d4-48ca-9dc2-72bbca1985ff"
/// category_name : "Pizza"

class CategoryModel {
  CategoryModel({
      String? docId,
      String? cateImage,
      String? categoryName,}){

    _docId = docId;
    _cateImage = cateImage;
    _categoryName = categoryName;
}

  CategoryModel.fromJson(DocumentSnapshot json) {

    _docId = json.id;
    _cateImage = json['cate_image'];
    _categoryName = json['category_name'];
  }
  String? _docId;
  String? _cateImage;
  String? _categoryName;

  String? get docId => _docId;
  String? get cateImage => _cateImage;
  String? get categoryName => _categoryName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _docId;
    map['cate_image'] = _cateImage;
    map['category_name'] = _categoryName;
    return map;
  }

}