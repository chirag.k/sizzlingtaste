import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:sizzlingtaste/App.dart';
import 'package:sizzlingtaste/UI/SignIn.dart';
import 'package:sizzlingtaste/constants/AppPreferenceField.dart';
import 'package:sizzlingtaste/resources/bottomNavigation.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await GetStorage.init();


  Widget _defaultWidget;
  late var userdata = GetStorage();

  if (userdata.read(AppPreferenceField.USER_MOBILE_NO) != null) {
    _defaultWidget = BottomNavigation();
  } else {
    _defaultWidget = SignIn();
  }

  // _defaultWidget =  DashBoard();
  // _defaultWidget =  SignIn();

  runApp(App(defaultWidgets: _defaultWidget));
}