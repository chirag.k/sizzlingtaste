import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizzlingtaste/constants/AppColor.dart';
import 'package:sizzlingtaste/constants/AppFontWeight.dart';
import 'package:sizzlingtaste/controller.dart';
import 'package:sizzlingtaste/fireBase/FirebaseController.dart';
import 'package:sizzlingtaste/resources/CustomButton.dart';
import 'package:sizzlingtaste/resources/CustomTextFiled.dart';
import 'package:sizzlingtaste/utility/Utilities.dart';


class AddCategoryInfo extends StatelessWidget {
  AddCategoryInfo({Key? key}) : super(key: key);
  HomeController control = Get.put(HomeController());
  FirebaseController fireControl = Get.put(FirebaseController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 8),
          child: SingleChildScrollView(
            child: Obx(()=>
                Column(
                children: [
                  Card(shadowColor: AppColor.colorBtnGreen,elevation:  5.0,
                    child: Container(
                      height: MediaQuery.of(context).size.height/3,
                      width: MediaQuery.of(context).size.width,
                      child: GestureDetector(
                        onTap: () {
                          control.showPicker(context,"category/");
                        },
                        child: Obx(() =>
                        control.selectedImagePath.value != ""
                            ? ClipRRect(child:Image.file(File(control.selectedImagePath.value),
                            width: MediaQuery.of(context).size.width/2,
                            height: MediaQuery.of(context).size.height/2,
                            fit: BoxFit.fill
                          ),
                        )
                            : Container(
                                  decoration: BoxDecoration(color: Colors.grey[200],),
                                  width: MediaQuery.of(context).size.width,
                                  height: MediaQuery.of(context).size.height / 2,
                                  child:
                                      // Image.network("https://i.pinimg.com/564x/e2/bc/2b/e2bc2b005d593253f62a4727d3da5d4f.jpg"),
                                      Icon(Icons.camera_alt,
                                          color: Colors.grey[800]),
                                ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 40),
                  Text(control.errorCategoryList[0],
                    style: Utilities.setTextStyle(AppFontWeight.titleText, AppFontWeight.semiBold,color: AppColor.colorError),),
                  const SizedBox(height: 30),

                  Obx(()=> CustomTextFiled(
                        textField: control.teAddProduct,
                        readOnly: false,
                        length: 100,
                        labelText: "Category name",
                        hintText: "Enter Your new category here",
                        errorText: control.errorCategoryList[1],
                        // backgroundColor:AppColor.colorUnderlineGray ,
                        onChange: (text){}),
                  ),
                  const SizedBox(height: 30),

                  CustomButton(
                      buttonTitle: "Add New Category",
                      backGroundColor: fireControl.completeUpload.value == false ?
                      AppColor.colorGray : (control.isCategoryValidate().value == true ? AppColor.colorError : AppColor.colorGray),

                      height: MediaQuery.of(context).size.height/15,
                      onTapButton: (){

                        if (control.isCategoryValidate().value == true) {
                          if (fireControl.completeUpload.value == true) {
                            fireControl.addNewCategory(control.teAddProduct.text,
                                fireControl.uploadFileUrl.value);
                            control.teAddProduct.clear();
                            fireControl.uploadFileUrl = ''.obs;
                            control.selectedImagePath = ''.obs;
                          } else {
                            Utilities.showSnackBar("Image uploading...", message: "Please wait");
                          }
                        }
                        else{
                          Utilities.showSnackBar("Please fill field", message: "Please wait");
                        }
                      }
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
