
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:sizzlingtaste/UI/addCategoryInfo.dart';
import 'package:sizzlingtaste/UI/products/AddProducts.dart';
import 'package:sizzlingtaste/UI/products/ShowProducts.dart';
import 'package:sizzlingtaste/constants/AppColor.dart';
import 'package:sizzlingtaste/constants/AppFontWeight.dart';
import 'package:sizzlingtaste/fireBase/FirebaseController.dart';
import 'package:sizzlingtaste/utility/Utilities.dart';

import '../controller.dart';

class CategoryAdd extends StatelessWidget {
  CategoryAdd({Key? key}) : super(key: key);


  final firebaseController = Get.put(FirebaseController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Category Add"),backgroundColor: AppColor.colorError),
      body: Obx(()=> ListView.builder(
          shrinkWrap: true,
            itemCount: firebaseController.allCategoryList.length,
            itemBuilder: (context, index){
              return viewCart(index);
            }),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: (){
            Get.to(()=> AddCategoryInfo());
          },
      child: const Icon(Icons.add,size: 24),),

    );
  }

  viewCart(int index) {
    return  Slidable(
        key: const ValueKey(0),
      endActionPane: ActionPane(
          motion: const DrawerMotion(),
          // dismissible: DismissiblePane(onDismissed: (){}),
          children: [
            SlidableAction(
              onPressed: (text){

              },
              backgroundColor: const Color(0xFFFE4A49),
              foregroundColor: Colors.white,
              icon: Icons.delete,
              label: 'Delete',
            ),
            SlidableAction(
              onPressed: (text){},
              backgroundColor: const Color(0xFF21B7CA),
              foregroundColor: Colors.white,
              icon: Icons.edit_rounded,
              label: 'Edit',
            ),
          ]),
      child: InkWell(
        onTap: () => Get.to(()=>ShowProducts(categoryName: firebaseController.allCategoryList[index].categoryName,)),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 5),
          child: Card(
            elevation: 3.0,
              shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
               Row(crossAxisAlignment: CrossAxisAlignment.center,
                 children: [
                   Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: ClipRRect(
                      borderRadius: BorderRadius.circular(50),
                      child: CachedNetworkImage(
                        imageUrl: firebaseController.allCategoryList[index].cateImage.toString(),
                        fit: BoxFit.cover,
                        progressIndicatorBuilder:
                            (context, url, downloadProgress) =>
                            CircularProgressIndicator(value: downloadProgress.progress),
                        errorWidget: (context, url, error) => Image.network(
                            'https://firebasestorage.googleapis.com/v0/b/sizzling-taste.appspot.com/o/fast-food-place-holder.jpg?alt=media&token=afa38e93-b624-4025-ad6e-a5896a7fa7b8'),
                        height: 70,
                        width: 70,
                      ),
                     ),
                   ),
                   Padding(
                     padding: const EdgeInsets.only(left:10,top: 0),
                     child: Text(firebaseController.allCategoryList[index].categoryName.toString(),style: Utilities.setTextStyle(AppFontWeight.titleSubText, AppFontWeight.semiBold),),
                   ),
                 ],
               ),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Icon(Icons.arrow_forward_ios,color: AppColor.colorGreen,size: 25),
              )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
