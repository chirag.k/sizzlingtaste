import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizzlingtaste/constants/AppFontWeight.dart';
import 'package:sizzlingtaste/constants/AppStrings.dart';
import 'package:sizzlingtaste/controller.dart';
import 'package:sizzlingtaste/fireBase/FirebaseController.dart';
import 'package:sizzlingtaste/resources/CustomButton.dart';
import 'package:sizzlingtaste/resources/CustomTextFiled.dart';
import 'package:sizzlingtaste/resources/Loader.dart';
import 'package:sizzlingtaste/utility/Utilities.dart';

import '../../constants/AppColor.dart';
import '../../resources/DecimalTextInputFormatter.dart';

class AddProducts extends StatelessWidget {
  AddProducts({Key? key,this.categoryName}) : super(key: key);

  String? categoryName;
  // FirebaseController firebaseController = Get.put(FirebaseController());

  @override
  Widget build(BuildContext context) {
    var controller = Get.put(HomeController());
    var firebaseController = Get.put(FirebaseController());
    controller.teProductCategory.text = categoryName.toString();
    firebaseController.getRestaurant();
    return Scaffold(
      appBar: AppBar(
          title: const Text("Add Product"),
          backgroundColor: AppColor.colorError),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Column(
                children: [
                  Obx(() => Card(
                      shadowColor: controller.selectVeg.value ? Colors.red : Colors.lightGreenAccent,
                      elevation: 15.0,
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0))),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(7),
                        child: Obx(()=> CachedNetworkImage(
                            imageUrl: firebaseController.uploadFileUrl.value.toString(),
                            fit: BoxFit.fill,
                            progressIndicatorBuilder:
                                (context, url, downloadProgress) =>
                                    CircularProgressIndicator(
                                        value: downloadProgress.progress),
                            errorWidget: (context, url, error) => Image.network(
                                'https://firebasestorage.googleapis.com/v0/b/sizzling-taste.appspot.com/o/fast-food-place-holder.jpg?alt=media&token=afa38e93-b624-4025-ad6e-a5896a7fa7b8'),
                            height: 200,
                            width: Get.size.width,
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 5),
                  InkWell(
                    onTap: () {
                      controller.showPicker(Get.context,"Products/");
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Text(
                        "Upload Image",
                        style: Utilities.setTextStyle(
                            AppFontWeight.titleSubText, AppFontWeight.semiBold,
                            textDecoration: TextDecoration.underline,color: Colors.lightBlue),
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  Obx(() => Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                              "Food Type :- ${controller.selectVeg.value ? AppStrings.nonVegetarian : AppStrings.vegetarian}",
                              style: Utilities.setTextStyle(
                                  AppFontWeight.titleSubText,
                                  AppFontWeight.semiBold)),
                          CupertinoSwitch(
                            value: controller.selectVeg.value,
                            trackColor: Colors.lightGreenAccent,
                            activeColor: Colors.red,
                            onChanged: (value) {
                              controller.selectVeg.value = value;

                            },
                          ),
                        ],
                      )),
                  const SizedBox(height: 10),
                  CustomTextFiled(
                    textField: controller.teProductName,
                    labelText: AppStrings.productName,
                    hintText: AppStrings.productName,
                    onChange: (text) {},
                    readOnly: false,
                    length: 255,
                    errorText: "",
                  ),
                  CustomTextFiled(
                    textField: controller.teProductCategory,
                    labelText: AppStrings.categoryName,
                    hintText: AppStrings.categoryName,
                    inputFormater: [DecimalTextInputFormatter(decimalRange: 2)],
                    onChange: (text) {},
                    readOnly: true,
                    length: 255,
                    errorText: "",
                  ),
                  CustomTextFiled(
                    textField: controller.teDescription,
                    labelText: AppStrings.description,
                    hintText: AppStrings.description,
                    onChange: (text) {},
                    maxLine: 4,
                    minLine: 1,
                    readOnly: false,
                    length: 100000,
                    errorText: "",
                  ),
                  CustomTextFiled(
                    textField: controller.tePrice,
                    labelText: AppStrings.productPrice,
                    hintText: "₹ "+AppStrings.productPrice,
                    inputFormater: [DecimalTextInputFormatter(decimalRange: 2)],
                    onChange: (text) {},
                    readOnly: false,
                    length: 10,
                    errorText: "",
                  ),

                  CustomButton(buttonTitle: AppStrings.save,
                      onTapButton: (){

                  })
                ],
              ),
              Obx(()=>firebaseController.isLoading.value == true ? Loader():Container())
            ],
          ),
        ),
      ),
    );
  }
}
