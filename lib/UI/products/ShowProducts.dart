
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizzlingtaste/UI/products/AddProducts.dart';

import '../../constants/AppColor.dart';
import '../../controller.dart';
import '../../utility/Utilities.dart';

class ShowProducts extends StatelessWidget {
  ShowProducts({Key? key,this.categoryName}) : super(key: key);

  String? categoryName;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Add Items"),backgroundColor: AppColor.colorError),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Utilities.showSnackBar('Done',snackbarPositonBottom: false);
          Get.to(()=> AddProducts(categoryName: categoryName));

        },
        child: const Icon(Icons.add,size: 24),),
    );
  }
}
