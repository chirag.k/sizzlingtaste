
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sizzlingtaste/UI/CreateAccountShop.dart';
import 'package:sizzlingtaste/UI/OtpScreen.dart';
import 'package:sizzlingtaste/constants/AppColor.dart';
import 'package:sizzlingtaste/constants/AppStrings.dart';
import 'package:sizzlingtaste/fireBase/FirebaseController.dart';
import 'package:sizzlingtaste/model/sideMenuDataModel.dart';
import 'package:sizzlingtaste/model/staticData.dart';
import 'package:sizzlingtaste/model/trendingProductModel.dart';
import 'package:sizzlingtaste/utility/Utilities.dart';
import 'package:sms_autofill/sms_autofill.dart';

import 'UI/DashBoard.dart';
import 'UI/addCategoryInfo.dart';
import 'UI/products/AddProducts.dart';
import 'constants/AppPreferenceField.dart';

class HomeController extends GetxController with GetSingleTickerProviderStateMixin, CodeAutoFill{


  // Create a CollectionReference called users that references the fireStore collection
  FirebaseFirestore fireStore  = FirebaseFirestore.instance;
  late  CollectionReference collectionReference;
  late FirebaseController firebaseController ;


  List <SideMenuDataModel> sideMenuData = <SideMenuDataModel> [].obs;

  RxBool selectVeg = false.obs;

  RxInt pageIndex = 0.obs;

  late final pages = [
    DashBoard(),
    AddCategoryInfo(),
    AddProducts(),
    OtpScreen(),
  ];

  List<String> errorMessageList = ['','','','','','','','',''].obs;
  List<String> errorCategoryList = ['',''].obs;

  final FirebaseAuth auth = FirebaseAuth.instance;
  final teMobileNo = TextEditingController();
  var teRestaurantName = TextEditingController();
  var teEmail = TextEditingController();
  var teAddress = TextEditingController();
  var teLandmark = TextEditingController();
  var teCity = TextEditingController();
  var teState = TextEditingController();
  var teCountry = TextEditingController();
  var tePinCode = TextEditingController();
  var teOtpTextController = TextEditingController();
  var teAddProduct = TextEditingController();

  // var tabIndex = 0;

  RxString phoneNoText = "".obs;
  // var isUpdate = "".obs;
  RxBool? isUpdate;
  var otpCode = "".obs;
  var verificationID = "".obs;


  late var userdata = GetStorage();
  List<TrendingProductModel> trendingProducts = [];
 RxBool isLoading = false.obs;    // loader 1/4

 loader(){                        // loader 2/4
   return const Center(
        child: CupertinoActivityIndicator(
            animating: true,radius: 30,color: AppColor.colorError)
    );
  }

  var selectedImagePath = "".obs;

  getImage(ImageSource imageSource, String path) async {
    final pickedFile = await ImagePicker().pickImage(source: imageSource);
    if(pickedFile != null){
      selectedImagePath.value = pickedFile.path;
      firebaseController.uploadImageTask(path, File(pickedFile.path));
    }else{
      Utilities.showError("ERROR",message: "No Image Selected");
    }
  }

  void showPicker(context, String path) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                      leading: const Icon(Icons.photo_library),
                      title: const Text('Photo Library'),
                      onTap: () {getImage(ImageSource.gallery,path);

                        // imgFromGallery();
                        Get.back();
                      }),
                  ListTile(
                    leading: const Icon(Icons.photo_camera),
                    title: const Text('Camera'),
                    onTap: () {getImage(ImageSource.camera,path);

                      // imgFromCamera();
                      Get.back();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  void onInit() {
    super.onInit();
    firebaseController = Get.put(FirebaseController());
    trendingProducts = getTrendingProducts();
    userdata = GetStorage();
    teOtpTextController.addListener(() {
      otpCode = RxString(teOtpTextController.text);
    });
    teMobileNo.addListener(() {
      phoneNoText.value = teMobileNo.text;
    });
    staticData();
    collectionReference = fireStore.collection('Restaurant');
  }


  @override
  void onClose(){
    super.onClose();
    teRestaurantName.dispose();
    teAddress.dispose();
    tePinCode.dispose();
    teEmail .dispose();
    teCity.dispose();
    teCountry.dispose();
    teLandmark.dispose();
    teState.dispose();
    teOtpTextController.dispose();
    otpCode.close();
  }

void sharedPrefWrite(String key,String value){
    userdata.write(key, value);
}

String sharedPrefRead(String key){
    return userdata.read(key);
}

  void getUserMobileNo(String key) async {
    Obx(() => phoneNoText = userdata.read(key));
  }

  void sharedPrefRemove(String key) {
    userdata.remove(key);
  }

  void sharedPrefEraseAllData() {
    userdata.erase();
  }

//side menuData list
 List <SideMenuDataModel> staticData(){
    sideMenuData.add(SideMenuDataModel("Home", Icons.home_filled));
    sideMenuData.add(SideMenuDataModel("Categories", Icons.category));
    sideMenuData.add(SideMenuDataModel("Favourites", Icons.favorite_border_rounded));
    sideMenuData.add(SideMenuDataModel("Order History", Icons.history));
    sideMenuData.add(SideMenuDataModel("Share", Icons.share));
    sideMenuData.add(SideMenuDataModel("LogOut", Icons.logout));
     return sideMenuData;
  }

  checkCreateAndUpdateScreen(RxBool isUpdateArg){
    if(isUpdateArg == true ) {
      isUpdate = RxString(AppStrings.updateAccount) as RxBool?;
          Get.to(CreateAccountShop());
    } else{
      RxString(AppStrings.createAccount);
      Get.offAll(() => CreateAccountShop());
    }
  }

  // for verify OTP and user in firebase auth.
  otpVerify() async {

    PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: verificationID.value, smsCode: otpCode.value.toString().trim());

    await FirebaseAuth.instance.signInWithCredential(credential).then((value) {

      // Utilities.showSnackBar(value.user!.phoneNumber.toString(),message: "Login Successfully Done");
      sharedPrefWrite(AppPreferenceField.USER_MOBILE_NO, value.user!.phoneNumber.toString());

      firebaseController.checkMobile(value.user!.phoneNumber.toString());
      // firebaseController.checkMobile

      print(value.user.toString());
    }).catchError((e) {
      if (e.message!.contains('network')) {
        Utilities.showSnackBar(AppStrings.checkInternetConnection);
      } else {
        Utilities.showError("Please fill correct OTP");
      }
    });
  }

  // For phone authentication and sent OTP
  verifyPhoneNo(String mobileNo){

    auth.verifyPhoneNumber(
      phoneNumber: '+91 '+mobileNo,
      timeout: Duration(seconds: 90),

      verificationCompleted: (PhoneAuthCredential credential) async {
         auth.signInWithCredential(credential);
      },

      codeSent: (String verificationId, int? resendToken) {
        verificationID = RxString(verificationId);
        Utilities.showSnackBar("OTP sent Successfully");

        Get.off(() => OtpScreen());
      },

      verificationFailed: (FirebaseAuthException error) {
        if(error.code == ''){
          print("Phone Number is incorrect");
        }
        if(error.code == 'invalid-phone-number') {
          Utilities.showError('The provided phone number is not valid.',message: "Please fill correct mobile no.");
        }

        if(error.code == 'too-many-requests') {
          Utilities.showError('Account is locked for 24 hours.',message: "Try again.");
        }

        if (error.message!.contains('network')) {
          Utilities.showError('Please check your internet connection and try again',
              message: "Please on your wifi/mobile data");
        }

      },

      codeAutoRetrievalTimeout: (String verificationId) {
        // Auto-resolution timed out...
      },

    );
  }

  RxBool isCategoryValidate() {
    RxBool validValue = true.obs;
    for(int v = 0; v< errorCategoryList.length; v++){
      if(v == 0){
        if(firebaseController.uploadFileUrl.value == ''){
          errorCategoryList[0] = 'Please Select category picture';
          validValue = false.obs;
        }
        else{
          errorCategoryList[0] = '';
        }
      } else if (v == 1){
        if(teAddProduct.text.trim().isEmpty){
          errorCategoryList[1] ='Please Enter category name';
          validValue =false.obs;
        }else{
          errorCategoryList[1] = '';
        }
      }
    }
    return validValue;
  }

  RxBool isValidate() {
    RxBool verify = true.obs;


    for (int v = 0; v < errorMessageList.length; v++) {
      if (v == 0) {
        if (teRestaurantName.text.toString().trim().isEmpty) {
          errorMessageList[0] = "Please Enter Restaurant Name";
          verify = false.obs;
        } else {
          errorMessageList[0] = "";
        }
      } else if (v == 1) {
        if (teEmail.text.toString().trim().isEmpty) {
          errorMessageList[1] = "Please enter eMail";
          verify = false.obs;
        } else {
          if(!GetUtils.isEmail(teEmail.text.toString())){
            errorMessageList[1] = "Email is not valid";
            verify = false.obs;
          }
          else
            errorMessageList[1] = "";
        }
      } else if (v == 2) {
        if (teAddress.text.toString().trim().isEmpty) {
          errorMessageList[2] = "Please enter your address";
          verify = false.obs;
        } else {
          errorMessageList[2] = "";
        }
      } else if (v == 3) {
        if (teLandmark.text.toString().trim().isEmpty) {
          errorMessageList[3] = "Please enter your Landmark";
          verify = false.obs;
        } else {
          errorMessageList[3] = "";
        }
      } else if (v == 4) {
        if (teCity.text.toString().trim().isEmpty) {
          errorMessageList[4] = "Please enter your city";
          verify = false.obs;
        } else {
          errorMessageList[4] = "";
        }
      } else if (v == 5) {
        if (teState.text.toString().trim().isEmpty) {
          errorMessageList[5] = "Please enter your state";
          verify = false.obs;
        } else {
          errorMessageList[5] = "";
        }
      } else if (v == 6) {
        if (teCountry.text.toString().trim().isEmpty) {
          errorMessageList[6] = "Please enter your Country";
          verify = false.obs;
        } else {
          errorMessageList[6] = "";
        }
      } else if (v == 7) {
        if (tePinCode.text.toString().trim().isEmpty) {
          errorMessageList[7] = "Please enter your Country";
          verify = false.obs;
        } else {
          errorMessageList[7] = "";
        }
      }
    }
    return verify;
  }

  //default function for autofill otp
  @override
  void codeUpdated() {
    otpCode = RxString(code!);
    teOtpTextController.text = code!;
  }

  var teProductName = TextEditingController();
  var tePrice = TextEditingController();
  var teDescription = TextEditingController();
  var teProductCategory = TextEditingController();

//   // Check If User is already Registered or not from firestore database
//   // var CheckFireData = FirebaseFirestore.instance.collection('Restaurant');
//   Future<void> checkMobileData(String mobNumber) async {
//     // var checkFireData = FirebaseFirestore.instance.collection('Restaurant').where(FieldPath.documentId,isEqualTo: mobNumber).get();
// var checkFireData = FirebaseFirestore.instance.collection('Restaurant').doc(mobNumber).get();
//
//     // final allData = querySnapshot.docs.map((doc) => doc.data()).toList();
//
//     print("sdsdsd=== $checkFireData");
//   }

  // getCategories(){
  //  var db = FirebaseFirestore.instance.collection("Restaurant");
  //
  //   FutureBuilder<DocumentSnapshot>(
  //      future: db.doc('+911234567890').get(),
  //      builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
  //        if(snapshot.hasError){
  //          Utilities.showError("Something went wrong");
  //        }
  //
  //        if(snapshot.hasData && !snapshot.data!.exists){
  //          Utilities.showError("Document does not exists");
  //        }
  //
  //        if(snapshot.connectionState == ConnectionState.done) {
  //          Map<String, dynamic> data = snapshot.data!.data() as Map<String, dynamic>;
  //          print(data);
  //          }
  //
  //        return Text('');
  //      });
  //
  // }

  // // Get Restaurant Data from FireStore
  // var getFireData = FirebaseFirestore.instance.collection('Restaurant');  // Get Restaurant Data from FireStore
  // Future<void> getData() async {
  //   // Get docs from collection reference
  //   QuerySnapshot querySnapshot = await getFireData.get();
  //
  //   // Get data from docs and convert map to List
  //   final allData = querySnapshot.docs.map((doc) => doc.data()).toList();
  //
  //   print("qqqqqq== $allData");
  // }


}
