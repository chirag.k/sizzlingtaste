import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizzlingtaste/constants/AppColor.dart';

import '../constants/AppColor.dart';
import '../controller.dart';

class BottomNavigation extends StatelessWidget {
  BottomNavigation({Key? key}) : super(key: key);

  HomeController controller = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {

    return Scaffold(

      // body: Obx(()=> contr.pages[contr.pageIndex.value]),
      body: Obx(()=>controller.pages[controller.pageIndex.value]),
      bottomNavigationBar: buildMyNavBar(context),
      backgroundColor: AppColor.colorTransparent
    );
  }
  Container buildMyNavBar(BuildContext context) {
    return Container(
      // margin: const EdgeInsets.only(left: 10,right: 10,bottom: 10),
      height: 57,
      decoration: const BoxDecoration(
        color: AppColor.colorBlack,
        borderRadius: BorderRadius.all(Radius.circular(7))
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Obx(()=>
             IconButton(
              enableFeedback: false,
              onPressed: () {
                controller.pageIndex.value = 0;
              },
              icon: controller.pageIndex.value == 0
                  ? const Icon(
                Icons.home_rounded,
                color: Colors.white,
                size: 28,
              )
                  : const Icon(
                Icons.home_outlined,
                color: Colors.white,
                size: 28,
              ),
            ),
          ),

          Obx(()=>
              IconButton(
                enableFeedback: false,
                onPressed: () {
                  controller.pageIndex.value = 2;
                },
                icon: controller.pageIndex.value == 2
                    ? const Icon(
                  Icons.widgets_rounded,
                  color: Colors.white,
                  size: 28,
                )
                    : const Icon(
                  Icons.widgets_outlined,
                  color: Colors.white,
                  size: 28,
                ),
              ),
          ),

              Padding(
                padding: const EdgeInsets.only(bottom: 25,right: 15),
                child: IconButton(
                  enableFeedback: false,
                  onPressed: () {
                    controller.pageIndex.value = 0;
                  },
                  icon: const Icon(
                    Icons.add_circle,
                    color: Colors.white,
                    size: 48,
                  )

                ),
              ),

          Obx(()=>
              IconButton(
                enableFeedback: false,
                onPressed: () {
                  controller.pageIndex.value = 1;
                },
                icon: controller.pageIndex.value == 1
                    ? const Icon(
                  Icons.shopping_bag_rounded,
                  color: Colors.white,
                  size: 28,
                )
                    : const Icon(
                  Icons.shopping_bag_outlined,
                  color: Colors.white,
                  size: 28,
                ),
              ),
          ),

          Obx(()=>
             IconButton(
              enableFeedback: false,
              onPressed: () {
                controller.pageIndex.value= 3;
              },
              icon: controller.pageIndex.value== 3
                  ? const Icon(
                Icons.person,
                color: Colors.white,
                size: 28,
              )
                  : const Icon(
                Icons.person_outline,
                color: Colors.white,
                size: 28,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
