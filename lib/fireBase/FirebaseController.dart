
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import '../constants/AppPreferenceField.dart';
import '../model/CategoryModel.dart';
import '../utility/Utilities.dart';
import 'WebFields.dart';

class FirebaseController extends GetxController {

  FirebaseFirestore fireStore  = FirebaseFirestore.instance;
  late var userdata = GetStorage();
  Reference firebaseStorage = FirebaseStorage.instance.ref();
  late  CollectionReference collectionReference;
  late RxList<CategoryModel> allCategoryList = RxList<CategoryModel>([]);
  var uploadFileUrl = ''.obs;
  var docID = "".obs;
  String userMobNo = '';
  var taskStatus = ''.obs;
  var uploadingFilePercentage = 0.0.obs;
  var isLoading = false.obs;
  var completeUpload = false.obs;


  @override
  void onInit() {
    super.onInit();
    print("Mobile no :: ${userdata.read(AppPreferenceField.USER_MOBILE_NO.toString())}");
    userMobNo = userdata.read(AppPreferenceField.USER_MOBILE_NO).toString();

    collectionReference = fireStore.collection('Restaurant');
    allCategoryList.bindStream(getAllCategory());

  }

  @override
  void dispose() {
    super.dispose();

  }
//asahsjasgashgzsjkhfkaUYGbfj
  String sharedPrefRead(String key){
    return userdata.read(key);
  }

  void sharedPrefWrite(String key,String value){
    userdata.write(key, value);
  }

// for checking user is register or no
   Future checkMobile(String mobile) async{
     var userNo = await collectionReference.doc("$mobile").get();
     if(userNo.exists){
       print('Exists');
       Utilities.showSnackBar("Welcome Back");
       Get.offAllNamed("/bottomNavigation");
       onInit();
       // Get.reset();
       return userNo;

     }
     if(!userNo.exists){
       print('Not exists');
       Utilities.showSnackBar("Please Register your Business");
       Get.offAllNamed('/createAccount');
       // Get.reset();
       return null;
     }
   }

  //Get Category data live firebase data fetch
  Stream<List<CategoryModel>> getAllCategory() {
      return collectionReference.doc("$userMobNo").collection("category").snapshots().map((data) {
        final allData = data.docs.map((doc) => doc.data()).toList();
        print("All Data:: ${allData.toString()}");

        return data.docs.map((e) => CategoryModel.fromJson(e)).toList();
      });
  }

  getRestaurant() async {

    var collection = fireStore.collection('Restaurant');
    var docSnapshot = await collection.doc("$userMobNo").get();
    if (docSnapshot.exists) {
      Map<String, dynamic>? data = docSnapshot.data();
      var value = data?[WebFields.RESTAURANT_NAME]; // <-- The value you want to retrieve.
      // Call setState if needed.
      sharedPrefWrite(WebFields.RESTAURANT_NAME, value.toString());
      print("restaurant name :: ${value.toString()}");
    }



  }

  //Get Category data live firebase data fetch
  // Stream<List<CategoryModel>> getRestaurant() {
  //
  //   return collectionReference.doc("$userMobNo").collection("category").snapshots().map((data) {
  //     final allData = data.docs.map((doc) => doc.data()).toList();
  //     print("All Data:: ${allData.toString()}");
  //
  //     return data.docs.map((e) => CategoryModel.fromJson(e)).toList();
  //   });
  // }

  // final allData = data.docs.map((doc) => doc.data()).toList();
  // print("All Data:: ${allData.toString()}");

  uploadImage(String path,File image) async {

    Reference ref = FirebaseStorage.instance.ref().child("$path/${image.toString().split("/").last}");
     await ref.putFile(File(image.path));
    // uploadFileUrl = await ref.getDownloadURL().toString().obs;
    uploadFileUrl = (await ref.getDownloadURL()).toString().obs;
    print("Image url :: ${uploadFileUrl.value}");
  }

  uploadImageTask(String path, File image) async{

    isLoading = true.obs;
    firebase_storage.UploadTask task = firebase_storage.FirebaseStorage.instance
        .ref("$path/${image.toString().split("/").last}").putFile(File(image.path));

    task.snapshotEvents.listen((event) {
      print('Task state: ${event.state}');
      print('Progress: ${(event.bytesTransferred / event.totalBytes) * 100} %');

    },onError: (e){

      print(task.snapshot);
      if (e.code == 'permission-denied') {
        print('User does not have permission to upload to this reference.');
      }

    });

    // We can still optionally use the Future alongside the stream.
    try {
      await task;
      print('Upload complete.');
      final snapshot = await task.whenComplete(() => {});
      final urlDownload = await snapshot.ref.getDownloadURL();

      print("larg link :: ${urlDownload.toString()}");
      uploadFileUrl = urlDownload.obs;
      completeUpload = true.obs;
      isLoading = false.obs;
    } on FirebaseException catch (e){
      if (e.code == 'permission-denied') {
        print('User does not have permission to upload to this reference.');
      }
    }
  }



  addNewCategory(String categoryName, String categoryImage){// ADD NEW CATEGORY FROM "CATEGORY ADD" SCREEN

    DateTime currentCategoryAddDate = DateTime.now();
    Timestamp categoryAddTime = Timestamp.fromDate(currentCategoryAddDate);
    Map<String, Object?> addData = toAddData(categoryName,categoryAddTime,categoryImage);
    collectionReference.doc("$userMobNo").collection("category").doc("$categoryName").set(addData);
    Utilities.showSnackBar("Successfully",message: "Category added successfully done");
  }

  Map<String, Object?> toAddData(String categoryName, Timestamp categoryAddTime, String catImage) => {
    WebFields.CATEGORY_NAME : categoryName,
    WebFields.DATE_TIME : categoryAddTime,
    WebFields.CATEGORY_IMAGE : catImage
  };

  addRestaurantData(String restaurantName,String email,String address,String landmark,String city,String state,
      String country,String pinCode,String mobileNo){
    DateTime currentPhoneDate = DateTime.now();
    Timestamp myTimeStamp = Timestamp.fromDate(currentPhoneDate); //To TimeStamp
    Map<String, Object?> requestParm = toHashMap(restaurantName, email, address,
        landmark, city, state, country, pinCode, mobileNo,myTimeStamp);

    collectionReference.doc(mobileNo).set(requestParm).then((value) {
      sharedPrefWrite(WebFields.RESTAURANT_NAME,restaurantName);
      return Utilities.showSnackBar("Data add successfully ");
    })
        .catchError((onError)=> Utilities.showError("Failed to add user: $onError"));
    print("MY DATA IS:-- $requestParm");

  }

  Map<String, Object> toHashMap(String restaurantName,String email,String address,
      String landmark,String city,String state,String country,String pinCode,String mobileNo,Timestamp myTimeStamp) => {
    WebFields.RESTAURANT_NAME : restaurantName,
    WebFields.EMAIL : email,
    WebFields.ADDRESS : address,
    WebFields.LANDMARK : landmark,
    WebFields.CITY : city,
    WebFields.STATE : state,
    WebFields.COUNTRY : country,
    WebFields.PINCODE : pinCode,
    WebFields.RESTAURANT_MOBILE : mobileNo,
    WebFields.DATE_TIME : myTimeStamp
  };

}